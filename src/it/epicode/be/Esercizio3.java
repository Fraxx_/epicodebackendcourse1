package it.epicode.be;

import java.util.Scanner;
@SuppressWarnings("resource")
public class Esercizio3 {
	
	static int perimetroRettangolo(int a, int b) {
		
		return (a*2)+(b*2);
		
	}
	
	static int pariDispari(int num) {
		
		if((num % 2) == 0) {
			return 0;
		}else {
			return 1;
		}
		
	}
	static double areaTriangolo(int a, int b, int c) {
		
		int p = (a+b+c)/2;
		
		double s = Math.sqrt((p) * (p-a) * (p-b)* (p-c));
		return s;
	}
	
	
	
	
public static void main(String[] args) {
		
	
	System.out.println("-----------------------");
	System.out.println("Perimetro Rettangolo");
	System.out.println("-----------------------");
	System.out.print("inserisci il primo valore: ");
	
	Scanner num1 = new Scanner(System.in);
	int numero1 = num1.nextInt();
	
    System.out.print("inserisci il secondo valore: ");
	
	Scanner num2 = new Scanner(System.in);
	int numero2 = num2.nextInt();
	
	
    int perimetro = perimetroRettangolo(numero1,numero2);

	
	System.out.println("il perimetro del rettangolo �: " + perimetro);
	
	
	System.out.println("-----------------------");
	System.out.println("Pari & Dispari");
	System.out.println("-----------------------");
	System.out.print("inserisci il numero: ");
	Scanner num3 = new Scanner(System.in);
	int numero3 = num3.nextInt();
	int parimpari = pariDispari(numero3);
	
	System.out.println("il numero inserito equivale a: " + parimpari);
	
	System.out.println("-----------------------");
	System.out.println("Area Triangolo");
	System.out.println("-----------------------");

	System.out.print("inserisci il primo numero: 1");
	Scanner num4 = new Scanner(System.in);
	int n1 = num4.nextInt();
	
	System.out.print("inserisci il secondo numero: ");
	Scanner num5 = new Scanner(System.in);
	int n2 = num5.nextInt();
	
	System.out.print("inserisci il terzo numero: ");
	Scanner num6 = new Scanner(System.in);
	int n3 = num6.nextInt();
	
	double area = areaTriangolo(n1,n2,n3);
	System.out.println(area);
	
	
}
	
	
	
	
}