package it.epicode.be;

public class Esercizio1 {
	
	
	
	static int moltiplica(int a, int b) {
		
		return a*b;
	}
	
	static String concatena(String a, int b) {
	
		return a + b;
	}
	
	static String [] inserisciInArray(String[] a, String b) {
		String [] result = new String[6];
	
		for(int i = 0; i <6; i ++) {
				
			if(i==3) {
				result[i] = b;	
			}
			else if(i>3) {
				result[i] = a[i-1];
			}
			else {
				result[i] = a[i];
			}
		}
	
		
		return result;
	}
	
	
	
public static void main(String[] args) {
		
	int a = moltiplica(4,3);
	System.out.println(a);
	
	String b = concatena("Stringa ",3);
	System.out.println(b);
	
	String [] data = {"1","2","3","4","5"};
	String [] g = inserisciInArray(data, "hello");
	for (int i = 0; i <g.length; i ++) {
		System.out.println(g[i]);
	}
	
	
	}
	
	
	
	
	
}
